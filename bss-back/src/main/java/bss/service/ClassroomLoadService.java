package bss.service;

import bss.dto.ClassroomLoadDto;
import bss.entity.ClassroomLoad;
import bss.entity.ClassroomWorkload;
import bss.entity.Teachers;
import bss.repository.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClassroomLoadService {

    private final ClassroomLoadRepository classroomLoadRepository;
    private final TeachersRepository teachersRepository;
    private final ClassroomWorkloadRepository classroomWorkloadRepository;

    public ClassroomLoadService(ClassroomLoadRepository classroomLoadRepository, TeachersRepository teachersRepository,
                                ClassroomWorkloadRepository classroomWorkloadRepository,
                                ClassroomLoadAutoRepository classroomLoadAutoRepository) {
        this.classroomLoadRepository = classroomLoadRepository;
        this.teachersRepository = teachersRepository;
        this.classroomWorkloadRepository = classroomWorkloadRepository;
    }

    public List<ClassroomLoadDto> getClassroomLoad() {
        return classroomLoadEntityToDto(classroomLoadRepository.findAll());
    }

    public List<ClassroomLoadDto> getClassroomLoadByFullNameTeacher(Long teacherId, String semestr) {
        List<ClassroomLoadDto> result;
        if (teacherId != -1 && !semestr.isEmpty()) {
            if (semestr.equals("Нечетный")) {
                result = classroomLoadEntityToDto(classroomLoadRepository.
                        findAllByTeacherAndSemestr(teachersRepository.getOne(teacherId), 1));
                result.addAll(classroomLoadEntityToDto(classroomLoadRepository.
                        findAllByTeacherAndSemestr(teachersRepository.getOne(teacherId), 3)));
                result.addAll(classroomLoadEntityToDto(classroomLoadRepository.
                        findAllByTeacherAndSemestr(teachersRepository.getOne(teacherId), 5)));
                return result;
            } else {
                result = classroomLoadEntityToDto(classroomLoadRepository.
                        findAllByTeacherAndSemestr(teachersRepository.getOne(teacherId), 2));
                result.addAll(classroomLoadEntityToDto(classroomLoadRepository.
                        findAllByTeacherAndSemestr(teachersRepository.getOne(teacherId), 4)));
                result.addAll(classroomLoadEntityToDto(classroomLoadRepository.
                        findAllByTeacherAndSemestr(teachersRepository.getOne(teacherId), 6)));
                return result;
            }
        } else if (teacherId != - 1) {
            return classroomLoadEntityToDto(classroomLoadRepository.findAllByTeacher(teachersRepository.getOne(teacherId)));
        } else if (!semestr.isEmpty()) {
            if (semestr.equals("Нечетный")) {
                result = classroomLoadEntityToDto(classroomLoadRepository.findAllBySemestr(1));
                result.addAll(classroomLoadEntityToDto(classroomLoadRepository.findAllBySemestr(3)));
                result.addAll(classroomLoadEntityToDto(classroomLoadRepository.findAllBySemestr(5)));
                return result;
            } else {
                result = classroomLoadEntityToDto(classroomLoadRepository.findAllBySemestr(2));
                result.addAll(classroomLoadEntityToDto(classroomLoadRepository.findAllBySemestr(4)));
                result.addAll(classroomLoadEntityToDto(classroomLoadRepository.findAllBySemestr(6)));
                return result;
            }
        } else {
            return classroomLoadEntityToDto(classroomLoadRepository.findAll());
        }
    }

    public Integer getAllHours(Long audienceWorkloadNameId, Integer typeLoadId) {
        Integer result;
        ClassroomWorkload audienceWorkloadEntity = classroomWorkloadRepository.getOne(audienceWorkloadNameId);
        switch (typeLoadId) {
            case 1:
                result = audienceWorkloadEntity.getLectures();
                break;
            case 2:
                result = audienceWorkloadEntity.getPractice();
                break;
            case 3:
                result = audienceWorkloadEntity.getLaboratoryLessons();
                break;
            default:
                result = 0;
        }
        return result;
    }

    public List<Integer> getAllSemestr(Long audienceWorkloadNameId) {
        List<Integer> result = new ArrayList<>();
        String nameOfDescipline = classroomWorkloadRepository.getOne(audienceWorkloadNameId).getNameOfDisciplines();
        List<ClassroomWorkload> audienceWorkloadEntityList =
                classroomWorkloadRepository.findAllByNameOfDisciplines(nameOfDescipline);

        for (ClassroomWorkload entity: audienceWorkloadEntityList) {
            result.add(entity.getSemestr());
        }

        return result;

    }

    public void addClassroomLoad(Long teacherFullNameId, Long audienceWorkloadNameId, String typeLoadName, Integer hour, Integer semestr) {
        String nameOfDescipline = classroomWorkloadRepository.getOne(audienceWorkloadNameId).getNameOfDisciplines();
        ClassroomLoad entity = new ClassroomLoad();
        entity.setTeacher(teachersRepository.getOne(teacherFullNameId));
        entity.setClassroomWorkload(classroomWorkloadRepository.findAllByNameOfDisciplinesAndSemestr(nameOfDescipline, semestr).get(0));
        entity.setTypeLoad(typeLoadName);
        entity.setHours(hour);
        entity.setSemestr(semestr);
        classroomLoadRepository.save(entity);
    }

    private List<ClassroomLoadDto> classroomLoadEntityToDto(List<ClassroomLoad> entityList) {
        List<ClassroomLoadDto> result = new ArrayList<>();
        for (ClassroomLoad entity : entityList) {
            ClassroomLoadDto dto = new ClassroomLoadDto();
            dto.setId(entity.getId());
            Optional<Teachers> teacher = teachersRepository.findById(entity.getTeacher().getId());
            teacher.ifPresent(teachers -> dto.setTeacher(teachers.getFullNmae()));
            Optional<ClassroomWorkload> classroomWorkload =
                    classroomWorkloadRepository.findById(entity.getClassroomWorkload().getId());
            classroomWorkload.ifPresent(
                    audienceWorkload1 -> dto.setClassroomWorkload(classroomWorkload.get().getNameOfDisciplines())
            );
            dto.setHours(entity.getHours());
            dto.setTypeLoad(entity.getTypeLoad());
            dto.setSemestr(entity.getSemestr());
            result.add(dto);
        }
        return result;
    }

}
