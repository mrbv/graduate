package bss.service;

import bss.dto.OutOfClassWorkloadDto;
import bss.entity.OutOfClassWorkload;
import bss.repository.OutOfClassWorkloadRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OutOfClassWorkloadService {

    private final OutOfClassWorkloadRepository outOfClassWorkloadRepository;

    public OutOfClassWorkloadService(OutOfClassWorkloadRepository outOfClassWorkloadRepository) {
        this.outOfClassWorkloadRepository = outOfClassWorkloadRepository;
    }

    public List<OutOfClassWorkloadDto> getOutOfClassWorkload() {
        List<OutOfClassWorkloadDto> result = new ArrayList<>();
        for (OutOfClassWorkload entity : outOfClassWorkloadRepository.findAll()) {
            OutOfClassWorkloadDto dto = new OutOfClassWorkloadDto();
            dto.setId(entity.getId());
            dto.setCurriculumDesignation(entity.getCurriculumDesignation());
            dto.setNameOfStudy(entity.getNameOfStudy());
            dto.setProfileProgramSpecializationFocus(entity.getProfileProgramSpecializationFocus());
            dto.setKindTrainingLoad(entity.getKindTrainingLoad());
            dto.setCourse(entity.getCourse());
            dto.setSemestr(entity.getSemestr());
            dto.setNumberGroups(entity.getNumberGroups());
            dto.setNumberSubgroups(entity.getNumberSubgroups());
            dto.setDays(entity.getDays());
            dto.setStrength(entity.getStrength());
            dto.setWeeks(entity.getWeeks());
            dto.setHours(entity.getHours());
            dto.setZet(entity.getZet());
            result.add(dto);
        }
        return result;
    }
}
