package bss.service;

import bss.dto.OutOfClassLoadDto;
import bss.entity.OutOfClassLoad;
import bss.entity.OutOfClassWorkload;
import bss.entity.Teachers;
import bss.repository.OutOfClassLoadRepository;
import bss.repository.OutOfClassWorkloadRepository;
import bss.repository.TeachersRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OutOfClassLoadService {

    private final OutOfClassLoadRepository outOfClassLoadRepository;
    private final OutOfClassWorkloadRepository outOfClassWorkloadRepository;
    private final TeachersRepository teachersRepository;

    public OutOfClassLoadService(OutOfClassLoadRepository outOfClassLoadRepository,
                                 OutOfClassWorkloadRepository outOfClassWorkloadRepository,
                                 TeachersRepository teachersRepository) {
        this.outOfClassLoadRepository = outOfClassLoadRepository;
        this.outOfClassWorkloadRepository = outOfClassWorkloadRepository;
        this.teachersRepository = teachersRepository;
    }

    public List<OutOfClassLoadDto> getOutOfClassLoad() {
        return classroomLoadEntityToDto(outOfClassLoadRepository.findAll());
    }

    public List<OutOfClassLoadDto> getOutOfClassLoadByFullNameTeacher(Long teacherId) {
        return classroomLoadEntityToDto(outOfClassLoadRepository.findAllByTeacher(teachersRepository.getOne(teacherId)));
    }

    public Integer getAllHours(Long outOfClassLoadNameId) {
        OutOfClassWorkload outOfClassWorkloadEntity = outOfClassWorkloadRepository.getOne(outOfClassLoadNameId);
        return outOfClassWorkloadEntity.getHours();
    }

    public List<Integer> getAllSemestr(Long outOfClassWorkloadNameId) {
        List<Integer> result = new ArrayList<>();
        String nameOfStudy= outOfClassWorkloadRepository.getOne(outOfClassWorkloadNameId).getNameOfStudy();
        List<OutOfClassWorkload> outOfClassWorkloadEntityList =
                outOfClassWorkloadRepository.findAllByNameOfStudy(nameOfStudy);

        for (OutOfClassWorkload entity: outOfClassWorkloadEntityList) {
            result.add((entity.getCourse() - 1) * 2 + entity.getSemestr());
        }

        return result;

    }

    public void addOutOfClassLoad(Long teacherFullNameId, Long outOfClassWorkloadNameId, Integer hour, Integer semestr) {
        String nameOfStudy = outOfClassWorkloadRepository.getOne(outOfClassWorkloadNameId).getNameOfStudy();
        OutOfClassLoad entity = new OutOfClassLoad();
        entity.setTeacher(teachersRepository.getOne(teacherFullNameId));
        if (semestr.equals(1) || semestr.equals(3) || semestr.equals(5)) {
            entity.setOutOfClassWorkload(outOfClassWorkloadRepository.findByNameOfStudyAndSemestr(nameOfStudy, 1));
        } else {
            entity.setOutOfClassWorkload(outOfClassWorkloadRepository.findByNameOfStudyAndSemestr(nameOfStudy, 2));
        }
        entity.setHours(hour);
        entity.setSemestr(semestr);
        outOfClassLoadRepository.save(entity);
    }

    private List<OutOfClassLoadDto> classroomLoadEntityToDto(List<OutOfClassLoad> entityList) {
        List<OutOfClassLoadDto> result = new ArrayList<>();
        for (OutOfClassLoad entity : entityList) {
            OutOfClassLoadDto dto = new OutOfClassLoadDto();
            dto.setId(entity.getId());
            Optional<Teachers> teacher = teachersRepository.findById(entity.getTeacher().getId());
            teacher.ifPresent(teachers -> dto.setTeacher(teachers.getFullNmae()));
            Optional<OutOfClassWorkload> audienceWorkload = outOfClassWorkloadRepository.findById(entity.getOutOfClassWorkload().getId());
            audienceWorkload.ifPresent(audienceWorkload1 -> dto.setOutOffClassLoad(audienceWorkload.get().getNameOfStudy()));
            dto.setHours(entity.getHours());
            dto.setSemestr(entity.getSemestr());
            result.add(dto);
        }
        return result;
    }

}
