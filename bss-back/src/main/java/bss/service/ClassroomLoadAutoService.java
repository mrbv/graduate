package bss.service;

import bss.dto.ClassroomLoadDto;
import bss.entity.AvailableLoad;
import bss.entity.ClassroomLoadAuto;
import bss.entity.ClassroomWorkload;
import bss.entity.Teachers;
import bss.repository.AvailableLoadRepository;
import bss.repository.ClassroomLoadAutoRepository;
import bss.repository.ClassroomWorkloadRepository;
import bss.repository.TeachersRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ClassroomLoadAutoService {

    private static final String LECTURE = "Лекции";
    private static final String PRACTICE = "Практические";
    private static final String LABORATORY = "Лабораторные";
    private static final String OFFSET = "Зачет/Промежуточные аттестации";
    private static final String EXAM = "Экзамен";

    private static final String TEACHER = "преподаватель";
    public static final int HOURS_PER_SEMESTR = 160; // 900

    private final ClassroomLoadAutoRepository classroomLoadAutoRepository;
    private final TeachersRepository teachersRepository;
    private final ClassroomWorkloadRepository classroomWorkloadRepository;
    private final AvailableLoadRepository availableLoadRepository;

    public ClassroomLoadAutoService(ClassroomLoadAutoRepository classroomLoadAutoRepository,
                                    TeachersRepository teachersRepository,
                                    ClassroomWorkloadRepository classroomWorkloadRepository,
                                    AvailableLoadRepository availableLoadRepository) {
        this.classroomLoadAutoRepository = classroomLoadAutoRepository;
        this.teachersRepository = teachersRepository;
        this.classroomWorkloadRepository = classroomWorkloadRepository;
        this.availableLoadRepository = availableLoadRepository;
    }

    /**
     * Получить данные полученные автоматическим алгоритмом
     * @return
     */
    public List<ClassroomLoadDto> getAutoLoad() {
        return classroomLoadAutoEntityToDto(classroomLoadAutoRepository.findAll());
    }

    public void deleteAll() {
        List<ClassroomLoadAuto> classroomLoadAutoList = classroomLoadAutoRepository.findAll();
        classroomLoadAutoRepository.deleteAll(classroomLoadAutoList);
    }

    /**
     * Метод для запуска автоматического построения нагрузки по алгоритму "Жадный алгоритм"
     */
    public void auto() {
        List<ClassroomWorkload> classroomWorkloadList = classroomWorkloadRepository.findAll();

        List<ClassroomWorkload> classroomWorkloadLectureList = getClassroomWorkloadTypedList(classroomWorkloadList, 1);
        List<ClassroomWorkload> classroomWorkloadPracticeList = getClassroomWorkloadTypedList(classroomWorkloadList, 2);
        List<ClassroomWorkload> classroomWorkloadLaboratoryList = getClassroomWorkloadTypedList(classroomWorkloadList, 3);

        Collections.sort(classroomWorkloadLectureList, Comparator.comparingInt(ClassroomWorkload::getLectures));
        Collections.sort(classroomWorkloadPracticeList, Comparator.comparingInt(ClassroomWorkload::getPractice));
        Collections.sort(classroomWorkloadLaboratoryList, Comparator.comparingInt(ClassroomWorkload::getLaboratoryLessons));

        distribution(classroomWorkloadLectureList);
        distribution(classroomWorkloadPracticeList);
        distribution(classroomWorkloadLaboratoryList);

    }

    private boolean checkAvailableWorkload(Teachers teacher, ClassroomWorkload classroomWorkload) {
        List<AvailableLoad> availableLoadList = availableLoadRepository.findAllByTeachers(teacher);
        /*
         * Преподаватель может вести только доступные предметы
         * Если у преподавателя не указаны доступные предметы, то доступны все
         */
        boolean availableLoadToTeacher = false;
        if (!availableLoadList.isEmpty()) {
            for (AvailableLoad availableLoad : availableLoadList) {
                if (classroomWorkload.getNameOfDisciplines().equals(availableLoad.getClassroomWorkloadName())) {
                    availableLoadToTeacher = true;
                }
            }
        } else {
            availableLoadToTeacher = true;
        }
        System.out.println("Тест" + teacher.getFullNmae() + " result: " + availableLoadToTeacher);
        return availableLoadToTeacher;
    }

    /**
     * 1 - лекции
     * 2 - практические
     * 3 - лабораторные
     */
    private List<ClassroomWorkload> getClassroomWorkloadTypedList(List<ClassroomWorkload> classroomWorkloadList, int intFlag) {
        List<ClassroomWorkload> result = new ArrayList<>();

        switch (intFlag) {
            case 1: {
                for (ClassroomWorkload entity : classroomWorkloadList) {
                    if (entity.getLectures() != null) {
                        result.add(entity);
                    }
                }
            }
            break;
            case 2: {
                for (ClassroomWorkload entity : classroomWorkloadList) {
                    if (entity.getPractice() != null) {
                        result.add(entity);
                    }
                }
            }
            break;
            case 3: {
                for (ClassroomWorkload entity : classroomWorkloadList) {
                    if (entity.getLaboratoryLessons() != null) {
                        result.add(entity);
                    }
                }
            }
            break;
            default:
                break;
        }

        return result;
    }

    private void saveEntity(ClassroomWorkload entity, Teachers teacher, String typeLoad) {
        ClassroomLoadAuto auto = new ClassroomLoadAuto();

        auto.setTeacher(teacher);
        auto.setClassroomWorkload(entity);
        auto.setSemestr(entity.getSemestr());

        switch (typeLoad) {
            case LECTURE:
                auto.setHours(entity.getLectures());
                break;
            case PRACTICE:
                auto.setHours(entity.getPractice());
                break;
            case LABORATORY:
                auto.setHours(entity.getLaboratoryLessons());
                break;
            case OFFSET:
                auto.setHours(entity.getOffset());
                break;
            case EXAM:
                auto.setHours(entity.getExam());
                break;
            default:
                break;
        }

        auto.setTypeLoad(typeLoad);

        classroomLoadAutoRepository.save(auto);
    }

    public List<ClassroomLoadDto> getClassroomLoadByFullNameTeacher(Long teacherId) {
        return classroomLoadAutoEntityToDto(classroomLoadAutoRepository.findAllByTeacher(teachersRepository.getOne(teacherId)));
    }

    private List<ClassroomLoadDto> classroomLoadAutoEntityToDto(List<ClassroomLoadAuto> entityList) {
        List<ClassroomLoadDto> result = new ArrayList<>();
        for (ClassroomLoadAuto entity : entityList) {
            ClassroomLoadDto dto = new ClassroomLoadDto();
            dto.setId(entity.getId());
            Optional<Teachers> teacher = teachersRepository.findById(entity.getTeacher().getId());
            teacher.ifPresent(teachers -> dto.setTeacher(teachers.getFullNmae()));
            Optional<ClassroomWorkload> classroomWorkload =
                    classroomWorkloadRepository.findById(entity.getClassroomWorkload().getId());
            classroomWorkload.ifPresent(
                    audienceWorkload1 -> dto.setClassroomWorkload(classroomWorkload.get().getNameOfDisciplines())
            );
            dto.setHours(entity.getHours());
            dto.setTypeLoad(entity.getTypeLoad());
            dto.setSemestr(entity.getSemestr());
            result.add(dto);
        }
        return result;
    }

    private void distribution(List<ClassroomWorkload> classroomWorkloadList) {
        List<Teachers> teachers = teachersRepository.findAll();

        for (ClassroomWorkload workload : classroomWorkloadList) {
            for (Teachers teacher : teachers) {
                if (!checkAvailableWorkload(teacher, workload)) {
                    continue;
                }

                /*
                 * нагрузка на преподавателя в зависимости от его ставки
                 *
                 * максимальное нагрузка на преподавателя при полной ставке
                 */
                int hoursPerSemestr = teacher.getHours() * HOURS_PER_SEMESTR/40;

                // лекции
                if (workload.getLectures() != null) {

                    //Если текущая + последующая нагрузка превышают допустимую, то переходим к следующему преподавателю
                    //Зачеты и экзамены назнаются тому, кто ведет лекции
                    int checkHours = (teacher.getAutoHours() + workload.getLectures());
                    if (hoursPerSemestr < checkHours) {
                        continue;
                    } else if (workload.getOffset() != null && hoursPerSemestr < (checkHours + workload.getOffset())) {
                        continue;
                    } else if (workload.getExam() != null && hoursPerSemestr < (checkHours + workload.getExam())) {
                        continue;
                    } else if (workload.getOffset() != null && workload.getExam() != null
                            && hoursPerSemestr < (checkHours + workload.getExam() + workload.getOffset())) {
                        continue;
                    }

                    //Ученая степень = преподаватель
                    boolean isTeacher = teacher.getDegree().trim().toLowerCase().equals(TEACHER);
                    if (isTeacher) {
                        continue;
                    }

                    //сохраняем автораспределение и количество часов на преподавателе
                    saveEntity(workload, teacher, LECTURE);
                    teacher.setAutoHours(teacher.getAutoHours() + workload.getLectures());
                    teachersRepository.save(teacher);
                    if (workload.getOffset() != null) {
                        saveEntity(workload, teacher, OFFSET);
                        teacher.setAutoHours(teacher.getAutoHours() + workload.getOffset());
                        teachersRepository.save(teacher);
                    }
                    if (workload.getExam() != null) {
                        saveEntity(workload, teacher, EXAM);
                        teacher.setAutoHours(teacher.getAutoHours() + workload.getExam());
                        teachersRepository.save(teacher);
                    }

                }

                // практические
                if (workload.getPractice() != null) {
                    //Если текущая + последующая нагрузка превышают допустимую, то переходим к следующему преподавателю
                    if (hoursPerSemestr < (teacher.getAutoHours() + workload.getPractice())) {
                        continue;
                    }

                    //сохраняем автораспределение и количество часов на преподавателе
                    saveEntity(workload, teacher, PRACTICE);
                    teacher.setAutoHours(teacher.getAutoHours() + workload.getPractice());
                    teachersRepository.save(teacher);
                }

                // лабораторные
                if (workload.getLaboratoryLessons() != null) {
                    //Если текущая + последующая нагрузка превышают допустимую, то переходим к следующему преподавателю
                    if (hoursPerSemestr < (teacher.getAutoHours() + workload.getLaboratoryLessons())) {
                        continue;
                    }

                    //сохраняем автораспределение и количество часов на преподавателе
                    saveEntity(workload, teacher, LABORATORY);
                    teacher.setAutoHours(teacher.getAutoHours() + workload.getLaboratoryLessons());
                    teachersRepository.save(teacher);
                }
            }
        }
    }

}
