package bss.service;

import bss.dto.AvailableLoadDto;
import bss.entity.AvailableLoad;
import bss.repository.AvailableLoadRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AvailableLoadService {

    private final AvailableLoadRepository availableLoadRepository;

    public AvailableLoadService(AvailableLoadRepository availableLoadRepository) {
        this.availableLoadRepository = availableLoadRepository;
    }

    public List<AvailableLoadDto> getAvailableLoad() {
        List<AvailableLoadDto> result = new ArrayList<>();
        List<AvailableLoad> entities = availableLoadRepository.findAll();

        for (AvailableLoad entity : entities) {
            AvailableLoadDto dto = new AvailableLoadDto();
            dto.setId(entity.getId());
            dto.setTeacherId(entity.getTeachers().getId());
            dto.setTeacherName(entity.getTeachers().getFullNmae());
            dto.setClassroomWorkloadName(entity.getClassroomWorkloadName());

            result.add(dto);
        }

        return result;
    }

}
