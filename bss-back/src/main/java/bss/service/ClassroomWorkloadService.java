package bss.service;

import bss.dto.ClassroomWorkloadDto;
import bss.entity.ClassroomWorkload;
import bss.repository.ClassroomWorkloadRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClassroomWorkloadService {

    private final ClassroomWorkloadRepository classroomWorkloadRepository;

    public ClassroomWorkloadService(ClassroomWorkloadRepository classroomWorkloadRepository) {
        this.classroomWorkloadRepository = classroomWorkloadRepository;
    }

    public List<ClassroomWorkloadDto> getAmmWorkload() {
        List<ClassroomWorkloadDto> result = new ArrayList<>();
        for (ClassroomWorkload entity : classroomWorkloadRepository.findAll()) {
            ClassroomWorkloadDto dto = new ClassroomWorkloadDto();
            dto.setCurriculumDesignation(entity.getCurriculumDesignation());
            dto.setProfileProgramSpecializationFocus(entity.getProfileProgramSpecializationFocus());
            dto.setEmptyColumn(entity.getEmptyColumn());
            dto.setNameOfDisciplines(entity.getNameOfDisciplines());
            dto.setKindOfDisciplines(entity.getKindOfDisciplines());
            dto.setLaborInput(entity.getLaborInput());
            dto.setIndependentWork(entity.getIndependentWork());
            dto.setAuditorsHours(entity.getAuditorsHours());
            dto.setZet(entity.getZet());
            dto.setLectures(entity.getLectures());
            dto.setPractice(entity.getPractice());
            dto.setLaboratoryLessons(entity.getLaboratoryLessons());
            dto.setSemestr(entity.getSemestr());
            dto.setOffset(entity.getOffset());
            dto.setExam(entity.getExam());
            result.add(dto);
        }
        return result;
    }

    public List<ClassroomWorkloadDto> getNameOfDisciplines() {
        List<ClassroomWorkloadDto> result = new ArrayList<>();
        for (ClassroomWorkload entity : classroomWorkloadRepository.findAll()) {
            ClassroomWorkloadDto dto = new ClassroomWorkloadDto();
            dto.setId(entity.getId());
            dto.setNameOfDisciplines(entity.getNameOfDisciplines());
            result.add(dto);
        }
        return result;
    }

}
