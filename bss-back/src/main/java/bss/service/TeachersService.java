package bss.service;

import bss.dto.TeachersDto;
import bss.entity.Teachers;
import bss.repository.TeachersRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TeachersService {

    private final TeachersRepository teachersRepository;

    public TeachersService(TeachersRepository teachersRepository) {
        this.teachersRepository = teachersRepository;
    }

    public List<TeachersDto> getTeachers() {
        List<TeachersDto> result = new ArrayList<>();
        for (Teachers teacher : teachersRepository.findAll()) {
            TeachersDto dto = new TeachersDto();
            dto.setId(teacher.getId());
            dto.setFullName(teacher.getFullNmae());
            dto.setDegree(teacher.getDegree());
            dto.setHours(teacher.getHours());
            result.add(dto);
        }
        return result;
    }

    public List<TeachersDto> getTeachersFullName() {
        List<TeachersDto> result = new ArrayList<>();
        for (Teachers teacher : teachersRepository.findAll()) {
            TeachersDto dto = new TeachersDto();
            dto.setId(teacher.getId());
            dto.setFullName(teacher.getFullNmae());
            result.add(dto);
        }
        return result;
    }

}
