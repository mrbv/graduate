package bss.service;

import bss.dto.InformationStudentsDto;
import bss.entity.InformationStudents;
import bss.repository.InformationStudentsRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InfromationStudentsService {

    private final InformationStudentsRepository informationStudentsRepository;

    public InfromationStudentsService(InformationStudentsRepository informationStudentsRepository) {
        this.informationStudentsRepository = informationStudentsRepository;
    }

    public List<InformationStudentsDto> getInformationStudents() {
        List<InformationStudentsDto> result = new ArrayList<>();
        for (InformationStudents entity : informationStudentsRepository.findAll()) {
            InformationStudentsDto dto = new InformationStudentsDto();
            dto.setCourse(entity.getCourse());
            dto.setReceiptYear(entity.getReceiptYear());
            dto.setNumberStudents(entity.getNumberStudents());
            dto.setNumberSubGroups(entity.getNumberSubGroups());
            result.add(dto);
        }
        return result;
    }
}
