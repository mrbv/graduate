package bss.dto;

public class OutOfClassLoadDto {

    private Long id;
    private String teacher;
    private String outOffClassLoad;
    private Integer hours;
    private Integer semestr;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getOutOffClassLoad() {
        return outOffClassLoad;
    }

    public void setOutOffClassLoad(String outOffClassWorkload) {
        this.outOffClassLoad = outOffClassWorkload;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public Integer getSemestr() {
        return semestr;
    }

    public void setSemestr(Integer semestr) {
        this.semestr = semestr;
    }
}
