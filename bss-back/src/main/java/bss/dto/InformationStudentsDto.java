package bss.dto;

public class InformationStudentsDto {

    private Integer course;
    private Integer receiptYear;
    private Integer numberStudents;
    private Integer numberSubGroups;

    public InformationStudentsDto() {};

    public InformationStudentsDto(Integer course, Integer receiptYear, Integer numberStudents, Integer numberSubGroups) {
        this.course = course;
        this.receiptYear = receiptYear;
        this.numberStudents = numberStudents;
        this.numberSubGroups = numberSubGroups;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public Integer getReceiptYear() {
        return receiptYear;
    }

    public void setReceiptYear(Integer receiptYear) {
        this.receiptYear = receiptYear;
    }

    public Integer getNumberStudents() {
        return numberStudents;
    }

    public void setNumberStudents(Integer numberStudents) {
        this.numberStudents = numberStudents;
    }

    public Integer getNumberSubGroups() {
        return numberSubGroups;
    }

    public void setNumberSubGroups(Integer numberSubGroups) {
        this.numberSubGroups = numberSubGroups;
    }

}
