package bss.dto;

public class ClassroomLoadDto {

    private Long id;
    private String teacher;
    private String classroomWorkload;
    private Integer hours;
    private String typeLoad;
    private Integer semestr;
    private Integer totalHours;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getClassroomWorkload() {
        return classroomWorkload;
    }

    public void setClassroomWorkload(String classroomWorkload) {
        this.classroomWorkload = classroomWorkload;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public String getTypeLoad() {
        return typeLoad;
    }

    public void setTypeLoad(String typeLoad) {
        this.typeLoad = typeLoad;
    }

    public Integer getSemestr() {
        return semestr;
    }

    public void setSemestr(Integer semestr) {
        this.semestr = semestr;
    }

    public Integer getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(Integer totalHours) {
        this.totalHours = totalHours;
    }
}
