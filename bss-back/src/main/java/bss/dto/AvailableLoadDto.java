package bss.dto;

public class AvailableLoadDto {

    private Long id;
    private Long teacherId;
    private String teacherName;
    private String ClassroomWorkloadName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getClassroomWorkloadName() {
        return ClassroomWorkloadName;
    }

    public void setClassroomWorkloadName(String classroomWorkloadName) {
        ClassroomWorkloadName = classroomWorkloadName;
    }

}
