package bss.dto;

public class ClassroomWorkloadDto {

    private Long id;
    private String curriculumDesignation;
    private String profileProgramSpecializationFocus;
    private String emptyColumn;
    private String nameOfDisciplines;
    private String kindOfDisciplines;
    private Integer laborInput;
    private Integer independentWork;
    private Integer auditorsHours;
    private Integer zet;
    private Integer lectures;
    private Integer practice;
    private Integer laboratoryLessons;
    private Integer semestr;
    private Integer offset;
    private Integer exam;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurriculumDesignation() {
        return curriculumDesignation;
    }

    public void setCurriculumDesignation(String curriculumDesignation) {
        this.curriculumDesignation = curriculumDesignation;
    }

    public String getProfileProgramSpecializationFocus() {
        return profileProgramSpecializationFocus;
    }

    public void setProfileProgramSpecializationFocus(String profileProgramSpecializationFocus) {
        this.profileProgramSpecializationFocus = profileProgramSpecializationFocus;
    }

    public String getEmptyColumn() {
        return emptyColumn;
    }

    public void setEmptyColumn(String emptyColumn) {
        this.emptyColumn = emptyColumn;
    }

    public String getNameOfDisciplines() {
        return nameOfDisciplines;
    }

    public void setNameOfDisciplines(String nameOfDisciplines) {
        this.nameOfDisciplines = nameOfDisciplines;
    }

    public String getKindOfDisciplines() {
        return kindOfDisciplines;
    }

    public void setKindOfDisciplines(String kindOfDisciplines) {
        this.kindOfDisciplines = kindOfDisciplines;
    }

    public Integer getLaborInput() {
        return laborInput;
    }

    public void setLaborInput(Integer laborInput) {
        this.laborInput = laborInput;
    }

    public Integer getIndependentWork() {
        return independentWork;
    }

    public void setIndependentWork(Integer independentWork) {
        this.independentWork = independentWork;
    }

    public Integer getAuditorsHours() {
        return auditorsHours;
    }

    public void setAuditorsHours(Integer auditorsHours) {
        this.auditorsHours = auditorsHours;
    }

    public Integer getZet() {
        return zet;
    }

    public void setZet(Integer zet) {
        this.zet = zet;
    }

    public Integer getLectures() {
        return lectures;
    }

    public void setLectures(Integer lectures) {
        this.lectures = lectures;
    }

    public Integer getPractice() {
        return practice;
    }

    public void setPractice(Integer practice) {
        this.practice = practice;
    }

    public Integer getLaboratoryLessons() {
        return laboratoryLessons;
    }

    public void setLaboratoryLessons(Integer laboratoryLessons) {
        this.laboratoryLessons = laboratoryLessons;
    }

    public Integer getSemestr() {
        return semestr;
    }

    public void setSemestr(Integer semestr) {
        this.semestr = semestr;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getExam() {
        return exam;
    }

    public void setExam(Integer exam) {
        this.exam = exam;
    }
}
