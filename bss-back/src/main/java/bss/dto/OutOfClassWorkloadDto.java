package bss.dto;

public class OutOfClassWorkloadDto {

    private Long id;
    private String curriculumDesignation;
    private String nameOfStudy;
    private String profileProgramSpecializationFocus;
    private String kindTrainingLoad;
    private Integer course;
    private Integer semestr;
    private Integer numberGroups;
    private Integer numberSubgroups;
    private Integer days;
    private Integer strength;
    private Integer weeks;
    private Integer hours;
    private Integer zet;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurriculumDesignation() {
        return curriculumDesignation;
    }

    public void setCurriculumDesignation(String curriculumDesignation) {
        this.curriculumDesignation = curriculumDesignation;
    }

    public String getNameOfStudy() {
        return nameOfStudy;
    }

    public void setNameOfStudy(String nameOfStudy) {
        this.nameOfStudy = nameOfStudy;
    }

    public String getProfileProgramSpecializationFocus() {
        return profileProgramSpecializationFocus;
    }

    public void setProfileProgramSpecializationFocus(String profileProgramSpecializationFocus) {
        this.profileProgramSpecializationFocus = profileProgramSpecializationFocus;
    }

    public String getKindTrainingLoad() {
        return kindTrainingLoad;
    }

    public void setKindTrainingLoad(String kindTrainingLoad) {
        this.kindTrainingLoad = kindTrainingLoad;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public Integer getSemestr() {
        return semestr;
    }

    public void setSemestr(Integer semestr) {
        this.semestr = semestr;
    }

    public Integer getNumberGroups() {
        return numberGroups;
    }

    public void setNumberGroups(Integer numberGroups) {
        this.numberGroups = numberGroups;
    }

    public Integer getNumberSubgroups() {
        return numberSubgroups;
    }

    public void setNumberSubgroups(Integer numberSubgroups) {
        this.numberSubgroups = numberSubgroups;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public Integer getWeeks() {
        return weeks;
    }

    public void setWeeks(Integer weeks) {
        this.weeks = weeks;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public Integer getZet() {
        return zet;
    }

    public void setZet(Integer zet) {
        this.zet = zet;
    }
}
