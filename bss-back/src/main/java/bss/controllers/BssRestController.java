package bss.controllers;

import bss.dto.*;
import bss.service.*;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class BssRestController {

    private final InfromationStudentsService infromationStudentsService;
    private final ClassroomWorkloadService classroomWorkloadService;
    private final OutOfClassWorkloadService outOfClassWorkloadService;
    private final TeachersService teachersService;
    private final AvailableLoadService availableLoadService;

    public BssRestController(InfromationStudentsService infromationStudentsService,
                             ClassroomWorkloadService classroomWorkloadService,
                             OutOfClassWorkloadService outOfClassWorkloadService,
                             TeachersService teachersService, AvailableLoadService availableLoadService) {
        this.infromationStudentsService = infromationStudentsService;
        this.classroomWorkloadService = classroomWorkloadService;
        this.outOfClassWorkloadService = outOfClassWorkloadService;
        this.teachersService = teachersService;
        this.availableLoadService = availableLoadService;
    }

    @RequestMapping("/students/group/information")
    public List<InformationStudentsDto> informationStudentsController() {
        return infromationStudentsService.getInformationStudents();
    }

    @RequestMapping("/classroom/workload")
    public List<ClassroomWorkloadDto> ammWorkloadController() {
        return classroomWorkloadService.getAmmWorkload();
    }

    @RequestMapping("/out/of/class/workload")
    public List<OutOfClassWorkloadDto> outOfClassWorkloadController() {
        return outOfClassWorkloadService.getOutOfClassWorkload();
    }

    @RequestMapping("/teachers")
    public List<TeachersDto> teachersController() {
        return teachersService.getTeachers();
    }

    @RequestMapping("/teachers/full/name")
    public List<TeachersDto> teachersFullNameController() {
        return teachersService.getTeachersFullName();
    }

    @RequestMapping("/available")
    public List<AvailableLoadDto> availableLoadController() {
        return availableLoadService.getAvailableLoad();
    }

}
