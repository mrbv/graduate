package bss.controllers;

import bss.dto.ClassroomLoadDto;
import bss.dto.ClassroomWorkloadDto;
import bss.entity.Teachers;
import bss.repository.TeachersRepository;
import bss.service.ClassroomLoadAutoService;
import bss.service.ClassroomLoadService;
import bss.service.ClassroomWorkloadService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ClassroomLoadController {

    private final ClassroomLoadService classroomLoadService;
    private final ClassroomWorkloadService classroomWorkloadService;
    private final ClassroomLoadAutoService classroomLoadAutoService;
    private final TeachersRepository teachersRepository;

    private final static String BASE_URL = "/classroom";

    public ClassroomLoadController(ClassroomLoadService classroomLoadService,
                                   ClassroomWorkloadService classroomWorkloadService,
                                   ClassroomLoadAutoService classroomLoadAutoService,
                                   TeachersRepository teachersRepository) {
        this.classroomLoadService = classroomLoadService;
        this.classroomWorkloadService = classroomWorkloadService;
        this.classroomLoadAutoService = classroomLoadAutoService;
        this.teachersRepository = teachersRepository;
    }

    @RequestMapping(BASE_URL + "/load")
    public List<ClassroomLoadDto> classroomLoadController() {
        return classroomLoadService.getClassroomLoad();
    }

    @RequestMapping(BASE_URL + "/load/{teacherFullNameId}/{semestr}")
    public List<ClassroomLoadDto> classroomLoadByFullNameTeacherController(@PathVariable Long teacherFullNameId, @PathVariable String semestr) {
        return classroomLoadService.getClassroomLoadByFullNameTeacher(teacherFullNameId, semestr);
    }

    @RequestMapping(BASE_URL + "/workload/hours/{audienceWorkloadNameId}/{typeLoadId}")
    public Integer hours(@PathVariable Long audienceWorkloadNameId, @PathVariable Integer typeLoadId) {
        return classroomLoadService.getAllHours(audienceWorkloadNameId, typeLoadId);
    }

    @RequestMapping(BASE_URL + "/load/add/{teacherFullNameId}/{audienceWorkloadNameId}/{typeLoadName}/{hour}/{semestr}")
    public HttpStatus addWorkLoad(@PathVariable Long teacherFullNameId, @PathVariable Long audienceWorkloadNameId,
                                  @PathVariable String typeLoadName, @PathVariable Integer hour, @PathVariable Integer semestr) {
        classroomLoadService.addClassroomLoad(teacherFullNameId, audienceWorkloadNameId, typeLoadName, hour, semestr);
        return HttpStatus.OK;
    }

    @RequestMapping(BASE_URL + "/workload/semestr/{audienceWorkloadNameId}")
    public List<Integer> semestr(@PathVariable Long audienceWorkloadNameId) {
        return classroomLoadService.getAllSemestr(audienceWorkloadNameId);
    }

    @RequestMapping(BASE_URL + "/workload/name")
    public List<ClassroomWorkloadDto> audienceWorkloadName() {
        return classroomWorkloadService.getNameOfDisciplines();
    }

    @RequestMapping(BASE_URL + "/auto")
    public HttpStatus autoController() {
        classroomLoadAutoService.auto();
        return HttpStatus.OK;
    }

    @RequestMapping(BASE_URL + "/auto/delete-all")
    public HttpStatus deleteAllController() {
        for(Teachers teacher : teachersRepository.findAll()) {
            teacher.setAutoHours(0);
            teachersRepository.save(teacher);
        }
        classroomLoadAutoService.deleteAll();
        return HttpStatus.OK;
    }

    @RequestMapping(BASE_URL + "/auto-load")
    public List<ClassroomLoadDto> autoLoadController() {
        return classroomLoadAutoService.getAutoLoad();
    }

    @RequestMapping(BASE_URL + "/auto/{teacherFullNameId}")
    public List<ClassroomLoadDto> autoClassroomLoadByFullNameTeacherController(@PathVariable Long teacherFullNameId) {
        return classroomLoadAutoService.getClassroomLoadByFullNameTeacher(teacherFullNameId);
    }

}
