package bss.controllers;

import bss.dto.OutOfClassLoadDto;
import bss.dto.OutOfClassWorkloadDto;
import bss.service.OutOfClassLoadService;
import bss.service.OutOfClassWorkloadService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class OutOfClassLoadController {

    private final OutOfClassLoadService outOfClassLoadService;
    private final OutOfClassWorkloadService outOfClassWorkloadService;

    private final static String BASE_URL = "/out-of-class";

    public OutOfClassLoadController(OutOfClassLoadService outOfClassLoadService,
                                    OutOfClassWorkloadService outOfClassWorkloadService) {
        this.outOfClassLoadService = outOfClassLoadService;
        this.outOfClassWorkloadService = outOfClassWorkloadService;
    }

    @RequestMapping(BASE_URL + "/load")
    public List<OutOfClassLoadDto> outOfClassLoadController() {
        return outOfClassLoadService.getOutOfClassLoad();
    }

    @RequestMapping(BASE_URL + "/load/{teacherFullNameId}")
    public List<OutOfClassLoadDto> outOfClassLoadByFullNameTeacherController(@PathVariable Long teacherFullNameId) {
        return outOfClassLoadService.getOutOfClassLoadByFullNameTeacher(teacherFullNameId);
    }

    @RequestMapping(BASE_URL + "/workload/hours/{audienceWorkloadNameId}")
    public Integer hours(@PathVariable Long audienceWorkloadNameId) {
        return outOfClassLoadService.getAllHours(audienceWorkloadNameId);
    }

    @RequestMapping(BASE_URL + "/load/add/{teacherFullNameId}/{audienceWorkloadNameId}/{hour}/{semestr}")
    public HttpStatus addWorkLoad(@PathVariable Long teacherFullNameId, @PathVariable Long audienceWorkloadNameId,
                                  @PathVariable Integer hour, @PathVariable Integer semestr) {
        outOfClassLoadService.addOutOfClassLoad(teacherFullNameId, audienceWorkloadNameId, hour, semestr);
        return HttpStatus.OK;
    }

    @RequestMapping(BASE_URL + "/workload/semestr/{audienceWorkloadNameId}")
    public List<Integer> semestr(@PathVariable Long audienceWorkloadNameId) {
        return outOfClassLoadService.getAllSemestr(audienceWorkloadNameId);
    }

    @RequestMapping(BASE_URL + "/workload/name")
    public List<OutOfClassWorkloadDto> outOfClassWorkloadName() {
        return outOfClassWorkloadService.getOutOfClassWorkload();
    }

}
