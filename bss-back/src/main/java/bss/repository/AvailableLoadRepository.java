package bss.repository;

import bss.entity.AvailableLoad;
import bss.entity.Teachers;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AvailableLoadRepository extends JpaRepository<AvailableLoad, Long> {

    List<AvailableLoad> findAllByTeachers(Teachers teacher);

}
