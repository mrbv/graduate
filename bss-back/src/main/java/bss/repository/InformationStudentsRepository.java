package bss.repository;

import bss.entity.InformationStudents;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InformationStudentsRepository extends JpaRepository<InformationStudents, Long> {
}
