package bss.repository;

import bss.entity.OutOfClassWorkload;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OutOfClassWorkloadRepository extends JpaRepository<OutOfClassWorkload, Long> {

    List<OutOfClassWorkload> findAllByNameOfStudy(String nameOfStudy);

    OutOfClassWorkload findByNameOfStudyAndSemestr(String nameOfStudy, Integer semestr);

}
