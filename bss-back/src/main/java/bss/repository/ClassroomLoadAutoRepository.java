package bss.repository;

import bss.entity.ClassroomLoadAuto;
import bss.entity.Teachers;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClassroomLoadAutoRepository extends JpaRepository<ClassroomLoadAuto, Long> {

    List<ClassroomLoadAuto> findAllByTeacher(Teachers teacher);

}
