package bss.repository;

import bss.entity.ClassroomLoad;
import bss.entity.Teachers;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClassroomLoadRepository extends JpaRepository<ClassroomLoad, Long> {

    List<ClassroomLoad> findAllByTeacherAndSemestr(Teachers teacher, Integer semestr);

    List<ClassroomLoad> findAllByTeacher(Teachers teacher);

    List<ClassroomLoad> findAllBySemestr(Integer semestr);
}
