package bss.repository;

import bss.entity.OutOfClassLoad;
import bss.entity.Teachers;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OutOfClassLoadRepository extends JpaRepository<OutOfClassLoad, Long> {

    List<OutOfClassLoad> findAllByTeacher(Teachers teacher);
}
