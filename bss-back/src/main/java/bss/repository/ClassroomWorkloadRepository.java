package bss.repository;

import bss.entity.ClassroomWorkload;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClassroomWorkloadRepository extends JpaRepository<ClassroomWorkload, Long> {

    List<ClassroomWorkload> findAllByNameOfDisciplines(String nameOfDisciplines);

    ClassroomWorkload findByNameOfDisciplinesAndSemestr(String nameOfDisciplines, Integer semestr);

    List<ClassroomWorkload> findAllByNameOfDisciplinesAndSemestr(String nameOfDisciplines, Integer semestr);

}
