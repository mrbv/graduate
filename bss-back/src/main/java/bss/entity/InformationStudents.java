package bss.entity;

import javax.persistence.*;

@Entity
@Table(name = "inf_stud")
public class InformationStudents {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "COURSE")
    private Integer course;

    @Column(name = "RECEIPT_YEAR")
    private Integer receiptYear;

    @Column(name = "NUMBER_STUDENTS")
    private Integer numberStudents;

    @Column(name = "NUMBER_SUB_GROUPS")
    private Integer numberSubGroups;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public Integer getReceiptYear() {
        return receiptYear;
    }

    public void setReceiptYear(Integer receiptYear) {
        this.receiptYear = receiptYear;
    }

    public Integer getNumberStudents() {
        return numberStudents;
    }

    public void setNumberStudents(Integer numberStudents) {
        this.numberStudents = numberStudents;
    }

    public Integer getNumberSubGroups() {
        return numberSubGroups;
    }

    public void setNumberSubGroups(Integer numberSubGroups) {
        this.numberSubGroups = numberSubGroups;
    }
}
