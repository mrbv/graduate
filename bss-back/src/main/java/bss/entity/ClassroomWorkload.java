package bss.entity;

import javax.persistence.*;

@Entity
@Table(name = "classroom_workload")
public class ClassroomWorkload {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "curriculum_designation")
    private String curriculumDesignation;

    @Column(name = "profile_program_specialization_focus")
    private String profileProgramSpecializationFocus;

    @Column(name = "empty_column")
    private String emptyColumn;

    @Column(name = "name_of_disciplines")
    private String nameOfDisciplines;

    @Column(name = "kind_of_disciplines")
    private String kindOfDisciplines;

    @Column(name = "labor_input")
    private Integer laborInput;

    @Column(name = "independent_work")
    private Integer independentWork;

    @Column(name = "auditors_hours")
    private Integer auditorsHours;

    @Column(name = "zet")
    private Integer zet;

    @Column(name = "lectures")
    private Integer lectures;

    @Column(name = "practice")
    private Integer practice;

    @Column(name = "laboratory_lessons")
    private Integer laboratoryLessons;

    @Column(name = "semestr")
    private Integer semestr;

    @Column(name = "offset")
    private Integer offset;

    @Column(name = "exam")
    private Integer exam;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurriculumDesignation() {
        return curriculumDesignation;
    }

    public void setCurriculumDesignation(String curriculumDesignation) {
        this.curriculumDesignation = curriculumDesignation;
    }

    public String getProfileProgramSpecializationFocus() {
        return profileProgramSpecializationFocus;
    }

    public void setProfileProgramSpecializationFocus(String profileProgramSpecializationFocus) {
        this.profileProgramSpecializationFocus = profileProgramSpecializationFocus;
    }

    public String getEmptyColumn() {
        return emptyColumn;
    }

    public void setEmptyColumn(String emptyColumn) {
        this.emptyColumn = emptyColumn;
    }

    public String getNameOfDisciplines() {
        return nameOfDisciplines;
    }

    public void setNameOfDisciplines(String nameOfDisciplines) {
        this.nameOfDisciplines = nameOfDisciplines;
    }

    public String getKindOfDisciplines() {
        return kindOfDisciplines;
    }

    public void setKindOfDisciplines(String kindOfDisciplines) {
        this.kindOfDisciplines = kindOfDisciplines;
    }

    public Integer getLaborInput() {
        return laborInput;
    }

    public void setLaborInput(Integer laborInput) {
        this.laborInput = laborInput;
    }

    public Integer getIndependentWork() {
        return independentWork;
    }

    public void setIndependentWork(Integer independentWork) {
        this.independentWork = independentWork;
    }

    public Integer getAuditorsHours() {
        return auditorsHours;
    }

    public void setAuditorsHours(Integer auditorsHours) {
        this.auditorsHours = auditorsHours;
    }

    public Integer getZet() {
        return zet;
    }

    public void setZet(Integer zet) {
        this.zet = zet;
    }

    public Integer getLectures() {
        return lectures;
    }

    public void setLectures(Integer lectures) {
        this.lectures = lectures;
    }

    public Integer getPractice() {
        return practice;
    }

    public void setPractice(Integer practice) {
        this.practice = practice;
    }

    public Integer getLaboratoryLessons() {
        return laboratoryLessons;
    }

    public void setLaboratoryLessons(Integer laboratoryLessons) {
        this.laboratoryLessons = laboratoryLessons;
    }

    public Integer getSemestr() {
        return semestr;
    }

    public void setSemestr(Integer semestr) {
        this.semestr = semestr;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getExam() {
        return exam;
    }

    public void setExam(Integer exam) {
        this.exam = exam;
    }

    //    @Override
//    public int compareTo(ClassroomWorkload o) {
//        return this.getLaborInput() - o.getLaborInput();
//    }
}
