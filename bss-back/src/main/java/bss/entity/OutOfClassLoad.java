package bss.entity;

import javax.persistence.*;

@Entity
@Table(name = "out_of_class_load")
public class OutOfClassLoad {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "out_of_class_load_id_seq")
    @SequenceGenerator(name="out_of_class_load_id_seq", sequenceName="out_of_class_load_id_seq", allocationSize=1)
    private Long id;

    @OneToOne (optional = false, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn (name = "teacher_id", referencedColumnName = "id")
    private Teachers teacher;

    @OneToOne (optional = false, cascade = CascadeType.ALL)
    @JoinColumn (name = "out_of_class_workload_id", referencedColumnName = "id")
    private OutOfClassWorkload outOfClassWorkload;

    @Column(name = "hours")
    private Integer hours;

    @Column(name = "semestr")
    private Integer semestr;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Teachers getTeacher() {
        return teacher;
    }

    public void setTeacher(Teachers teacher) {
        this.teacher = teacher;
    }

    public OutOfClassWorkload getOutOfClassWorkload() {
        return outOfClassWorkload;
    }

    public void setOutOfClassWorkload(OutOfClassWorkload outOfClassWorkload) {
        this.outOfClassWorkload = outOfClassWorkload;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public Integer getSemestr() {
        return semestr;
    }

    public void setSemestr(Integer semestr) {
        this.semestr = semestr;
    }
}
