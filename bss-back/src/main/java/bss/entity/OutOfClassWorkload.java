package bss.entity;

import javax.persistence.*;

@Entity
@Table(name = "out_of_class_workload")
public class OutOfClassWorkload {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "curriculum_designation")
    private String curriculumDesignation;

    @Column(name = "name_of_study")
    private String nameOfStudy;

    @Column(name = "profile_program_specialization_focus")
    private String profileProgramSpecializationFocus;

    @Column(name = "kind_training_load")
    private String kindTrainingLoad;

    @Column(name = "course")
    private Integer course;

    @Column(name = "semestr")
    private Integer semestr;

    @Column(name = "number_groups")
    private Integer numberGroups;

    @Column(name = "number_subgroups")
    private Integer numberSubgroups;

    @Column(name = "days")
    private Integer days;

    @Column(name = "strength")
    private Integer strength;

    @Column(name = "weeks")
    private Integer weeks;

    @Column(name = "hours")
    private Integer hours;

    @Column(name = "zet")
    private Integer zet;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurriculumDesignation() {
        return curriculumDesignation;
    }

    public void setCurriculumDesignation(String curriculumDesignation) {
        this.curriculumDesignation = curriculumDesignation;
    }

    public String getNameOfStudy() {
        return nameOfStudy;
    }

    public void setNameOfStudy(String nameOfStudy) {
        this.nameOfStudy = nameOfStudy;
    }

    public String getProfileProgramSpecializationFocus() {
        return profileProgramSpecializationFocus;
    }

    public void setProfileProgramSpecializationFocus(String profileProgramSpecializationFocus) {
        this.profileProgramSpecializationFocus = profileProgramSpecializationFocus;
    }

    public String getKindTrainingLoad() {
        return kindTrainingLoad;
    }

    public void setKindTrainingLoad(String kindTrainingLoad) {
        this.kindTrainingLoad = kindTrainingLoad;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public Integer getSemestr() {
        return semestr;
    }

    public void setSemestr(Integer semestr) {
        this.semestr = semestr;
    }

    public Integer getNumberGroups() {
        return numberGroups;
    }

    public void setNumberGroups(Integer numberGroups) {
        this.numberGroups = numberGroups;
    }

    public Integer getNumberSubgroups() {
        return numberSubgroups;
    }

    public void setNumberSubgroups(Integer numberSubgroups) {
        this.numberSubgroups = numberSubgroups;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public Integer getWeeks() {
        return weeks;
    }

    public void setWeeks(Integer weeks) {
        this.weeks = weeks;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public Integer getZet() {
        return zet;
    }

    public void setZet(Integer zet) {
        this.zet = zet;
    }

    @Override
    public String toString() {
        return "OutOfClassWorkload{" +
                "id=" + id +
                ", curriculumDesignation='" + curriculumDesignation + '\'' +
                ", nameOfStudy='" + nameOfStudy + '\'' +
                ", profileProgramSpecializationFocus='" + profileProgramSpecializationFocus + '\'' +
                ", kindTrainingLoad='" + kindTrainingLoad + '\'' +
                ", course=" + course +
                ", semestr=" + semestr +
                ", numberGroups=" + numberGroups +
                ", numberSubgroups=" + numberSubgroups +
                ", days=" + days +
                ", strength=" + strength +
                ", weeks=" + weeks +
                ", hours=" + hours +
                ", zet=" + zet +
                '}';
    }
}
