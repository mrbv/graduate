package bss.entity;

import javax.persistence.*;

@Entity
@Table(name = "classroom_load_auto")
public class ClassroomLoadAuto {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "classroom_load_auto_id_seq")
    @SequenceGenerator(name="classroom_load_auto_id_seq", sequenceName="classroom_load_auto_id_seq", allocationSize=1)
    private Long id;

    @OneToOne (optional = false, fetch = FetchType.LAZY)
    @JoinColumn (name = "teacher_id", referencedColumnName = "id")
    private Teachers teacher;

    @OneToOne (optional = false)
    @JoinColumn (name = "classroom_workload_id", referencedColumnName = "id")
    private ClassroomWorkload classroomWorkload;

    @Column(name = "hours")
    private Integer hours;

    @Column(name = "type_load")
    private String typeLoad;

    @Column(name = "semestr")
    private Integer semestr;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Teachers getTeacher() {
        return teacher;
    }

    public void setTeacher(Teachers teacher) {
        this.teacher = teacher;
    }

    public ClassroomWorkload getClassroomWorkload() {
        return classroomWorkload;
    }

    public void setClassroomWorkload(ClassroomWorkload classroomWorkload) {
        this.classroomWorkload = classroomWorkload;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public String getTypeLoad() {
        return typeLoad;
    }

    public void setTypeLoad(String typeLoad) {
        this.typeLoad = typeLoad;
    }

    public Integer getSemestr() {
        return semestr;
    }

    public void setSemestr(Integer semestr) {
        this.semestr = semestr;
    }

}
