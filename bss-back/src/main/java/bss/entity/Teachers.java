package bss.entity;

import javax.persistence.*;

@Entity
@Table(name = "teachers")
public class Teachers {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "full_name")
    private String fullNmae;

    @Column(name = "degree")
    private String degree;

    @Column(name = "hours")
    private Integer hours;

    @Column(name = "auto_hours")
    private Integer autoHours;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullNmae() {
        return fullNmae;
    }

    public void setFullNmae(String fullNmae) {
        this.fullNmae = fullNmae;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public Integer getAutoHours() {
        return autoHours;
    }

    public void setAutoHours(Integer autoHours) {
        this.autoHours = autoHours;
    }
}
