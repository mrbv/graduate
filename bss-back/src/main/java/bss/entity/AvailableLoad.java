package bss.entity;

import javax.persistence.*;

@Entity
@Table(name = "available_load")
public class AvailableLoad {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "available_load_id_seq")
    @SequenceGenerator(name="available_load_id_seq", sequenceName="available_load_id_seq", allocationSize=1)
    private Long id;

    @OneToOne (optional = false, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn (name = "teacher_id", referencedColumnName = "id")
    private Teachers teachers;

    @Column(name = "classroom_workload_name")
    private String classroomWorkloadName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Teachers getTeachers() {
        return teachers;
    }

    public void setTeachers(Teachers teachers) {
        this.teachers = teachers;
    }

    public String getClassroomWorkloadName() {
        return classroomWorkloadName;
    }

    public void setClassroomWorkloadName(String classroomWorkloadName) {
        this.classroomWorkloadName = classroomWorkloadName;
    }
}
