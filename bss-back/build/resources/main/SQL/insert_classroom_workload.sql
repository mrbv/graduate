/*1*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
  profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
  empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
  kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
  independent_work /*индивидуальные занятия*/,
  auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
  practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
                                   "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.Б.17',
        'Языки и методы программирования', null, null, 15, null, null, 17, 32, 102,
        3, 13, 18);

/*2*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
  profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
  empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
  kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
  independent_work /*индивидуальные занятия*/,
  auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
  practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
                                   "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.Б.17',
        'Языки и методы программирования', null, null, 10, null, null, null, null, 68,
        3, null, null);

/*3*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
  profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
  empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
  kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
  independent_work /*индивидуальные занятия*/,
  auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
  practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
                                   "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.Б.17',
        'Языки и методы программирования', null, null, 5, null, null, null, null, 34,
        3, null, null);

/*4*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
  profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
  empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
  kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
  independent_work /*индивидуальные занятия*/,
  auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
  practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
                                   "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.Б.17',
        'Языки и методы программирования', null, null, null, null, null, 17, 32, null,
        3, 13, 18);

/*5*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
                                   profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
                                   empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
                                   kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
                                   independent_work /*индивидуальные занятия*/,
                                   auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
                                   practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
                                   "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.Б.17',
        'Языки и методы программирования', null, null, 10, null, null, null, null, 68,
        3, null, null);

/*6*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
  profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
  empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
  kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
  independent_work /*индивидуальные занятия*/,
  auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
  practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
                                   "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.Б.17',
        'Языки и методы программирования', null, null, null, null, null, 16, null, null,
        3, null, 18);

/*7*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
  profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
  empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
  kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
  independent_work /*индивидуальные занятия*/,
  auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
  practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
                                   "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.Б.17',
        'Языки и методы программирования', null, null, 15, null, null, null, 64, 96,
        3, null, null);

/*8*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
                                   profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
                                   empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
                                   kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
                                   independent_work /*индивидуальные занятия*/,
                                   auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
                                   practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
                                   "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.Б.17',
        'Языки и методы программирования', null, null, 10, null, null, null, null, 64,
        3, 25, null);


/*9*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
                                   profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
                                   empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
                                   kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
                                   independent_work /*индивидуальные занятия*/,
                                   auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
                                   practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
                                   "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.Б.17',
        'Языки и методы программирования', null, null, 5, null, null, null, null, 32,
        3, null, null);

/*10*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
                                   profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
                                   empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
                                   kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
                                   independent_work /*индивидуальные занятия*/,
                                   auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
                                   practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
                                   "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.Б.17',
        'Языки и методы программирования', null, null, 10, null, null, null, null, 64,
        3, null, null);



/*11*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
  profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
  empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
  kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
  independent_work /*индивидуальные занятия*/,
  auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
  practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
                                   "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.Б.17',
        'Языки и методы программирования', null, null, null, null, null, 16, 64, null,
        3, null, 18);
------------------------------------------------------------------------------------------------------------------------
/*12*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
  profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
  empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
  kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
  independent_work /*индивидуальные занятия*/,
  auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
  practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
  "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.Б.18',
        'Базы данных', null, null, null, null, null, 34, null, 56,
        5, 13, null);

/*13*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
  profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
  empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
  kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
  independent_work /*индивидуальные занятия*/,
  auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
  practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
  "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.Б.18',
        'Базы данных', null, null, null, null, null, 34, null, 56,
        5, 13, null);

/*14*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
  profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
  empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
  kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
  independent_work /*индивидуальные занятия*/,
  auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
  practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
  "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.Б.18',
        'Базы данных', null, null, null, null, null, 16, null, 128,
        6, 15, 20);

/*15*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
                                   profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
                                   empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
                                   kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
                                   independent_work /*индивидуальные занятия*/,
                                   auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
                                   practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
                                   "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.Б.18',
        'Базы данных', null, null, null, null, null, 16, null, 96,
        6, 11, 15);
------------------------------------------------------------------------------------------------------------------------
/*16*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
  profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
  empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
  kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
  independent_work /*индивидуальные занятия*/,
  auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
  practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
  "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.В.ДВ.02.01',
        'Объектно-ориентированное программирование', null, null, null, null, null,
        null, null, 34, 3, null, null);

/*17*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
  profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
  empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
  kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
  independent_work /*индивидуальные занятия*/,
  auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
  practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
  "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.В.ДВ.02.01',
        'Объектно-ориентированное программирование', null, null, null, null, null,
        16, 64, 94, 3, 25, null);

/*18*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
  profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
  empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
  kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
  independent_work /*индивидуальные занятия*/,
  auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
  practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
  "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.В.ДВ.04.01',
        'Операционная система Windows', null, null, null, null, null,
        null, null, 136, 5, null, null);

/*19*/
INSERT INTO bss.classroom_workload(id, curriculum_designation /*учебный план*/,
                                   profile_program_specialization_focus /*Профиль, программа, специализация, направленность*/,
                                   empty_column /*Первая часть дисциплины*/, name_of_disciplines /*вторая часть дисциплины*/,
                                   kind_of_disciplines /*???*/, labor_input /*общее число часов*/,
                                   independent_work /*индивидуальные занятия*/,
                                   auditors_hours /*лекции + практики + лабораторные*/, zet /*???*/, lectures /*лекции*/,
                                   practice /*практические*/, laboratory_lessons /*лабораторные*/, semestr /*семестр*/,
                                   "offset" /*зачет, промежуточные аттестации*/, exam /*экзамен*/)
VALUES (nextval('bss.classroom_workload_id_seq'),
        'ПММ 01.03.02 Прикладная математика и информатика
Бакалавр (ФГОС3плюс)', 'Базовый блок дисциплин', 'Б1.В.ДВ.04.01',
        'Операционная система Windows', null, null, null, null, null,
        16, null, null, 5, 13, null);