export interface ClassroomWorkloadDto {
  id: number;
  curriculumDesignation: string;
  profileProgramSpecializationFocus: string;
  emptyColumn: string;
  nameOfDisciplines: string;
  kindOfDisciplines: string;
  laborInput: number;
  independentWork: number;
  auditorsHours: number;
  zet: number;
  lectures: number;
  practice: number;
  laboratoryLessons: number;
  semestr: number;
  offset: number;
  exam: number;
}
