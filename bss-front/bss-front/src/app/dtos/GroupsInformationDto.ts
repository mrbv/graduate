export interface GroupsInformationDto {
  receiptYear: number;
  course: number;
  numberStudents: number;
  numberSubGroups: number;
}
