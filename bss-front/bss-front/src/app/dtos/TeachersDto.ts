export interface TeachersDto {
  id: number,
  fullName: string,
  degree: string,
  hours: number
}
