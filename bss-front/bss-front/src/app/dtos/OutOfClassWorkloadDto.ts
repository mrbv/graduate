export interface OutOfClassWorkloadDto {
  id: number;
  curriculumDesignation: string;
  profileProgramSpecializationFocus: string;
  nameOfStudy: string;
  kindTrainingLoad: string;
  course: number;
  semestr: number;
  strength: number;
  numberGroups: number;
  numberSubgroups: number;
  days: number;
  weeks: number;
  hours: number;
  zet: number;
}
