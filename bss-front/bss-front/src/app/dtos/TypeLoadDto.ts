export interface TypeLoadDto {
  id: number,
  nameTypeLoad: string
}

export const typeLoadStub: TypeLoadDto[] = [
  {id: 1, nameTypeLoad: 'Лекции'},
  {id: 2, nameTypeLoad: 'Практические'},
  {id: 3, nameTypeLoad: 'Лабораторные'}
  ];
