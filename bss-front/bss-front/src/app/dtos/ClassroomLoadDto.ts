export interface ClassroomLoadDto {
  id: number,
  teacher: string,
  classroomWorkload: string,
  hours: number,
  typeLoad: string
}
