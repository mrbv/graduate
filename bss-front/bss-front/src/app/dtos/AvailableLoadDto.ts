export interface AvailableLoadDto {
  id: number,
  teacherId: number;
  teacherName: string,
  classroomWorkloadName: string
}
