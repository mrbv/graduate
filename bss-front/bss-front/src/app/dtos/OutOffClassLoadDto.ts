export interface OutOffClassLoadDto {
  id: number,
  teacher: string,
  outOffClassLoad: string,
  hours: number,
  typeLoad: string
}
