import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OutOfClassLoadAddDialogComponent} from './out-of-class-load-add-dialog.component';

describe('OutOfClassLoadAddDialogComponent', () => {
  let component: OutOfClassLoadAddDialogComponent;
  let fixture: ComponentFixture<OutOfClassLoadAddDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutOfClassLoadAddDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutOfClassLoadAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
