import {Component, OnInit} from '@angular/core';
import {TeachersDto} from "../../dtos/TeachersDto";
import {FormControl} from "@angular/forms";
import {OutOfClassLoadService} from "../../services/out-of-class-load.service";
import {OutOfClassWorkloadDto} from "../../dtos/OutOfClassWorkloadDto";
import {MatDialogRef} from "@angular/material/dialog";
import {RestService} from "../../services/rest.service";

@Component({
  selector: 'out-of-class-load-add-dialog',
  templateUrl: './out-of-class-load-add-dialog.component.html',
  styleUrls: ['./out-of-class-load-add-dialog.component.css']
})
export class OutOfClassLoadAddDialogComponent implements OnInit {

  teachers: TeachersDto[];
  outOfClassLoad: OutOfClassWorkloadDto[];
  semestr: number[];

  selectedTeacher: TeachersDto;
  selectedValueOutOfClassLoad: OutOfClassWorkloadDto;
  selectedHour: number;
  selectedSemestr: number;

  hours: number;

  outOfClassLoadControl = new FormControl();

  constructor(private outOfClassLoadService: OutOfClassLoadService, private restService: RestService,
              public dialogRef: MatDialogRef<OutOfClassLoadAddDialogComponent>) { }

  ngOnInit(): void {
    // фио преподавателей
    this.restService.getFullNameTeachers().subscribe(
      data => {
        this.teachers = data;
      }
    );

    // наименования дисциплин
    this.outOfClassLoadService.getNameOutOfClassWorkload().subscribe(
      data => {
        this.outOfClassLoad = data;
      }
    );
  }

  outOfClassLoadChange() {
    // запрос часов по выбранным параметрам
    if (this.outOfClassLoadControl.dirty) {
      this.selectedSemestr = null;
      this.outOfClassLoadService.getSemestr(this.selectedValueOutOfClassLoad.id).subscribe(
        data => {
          console.log(data);
          this.semestr = data;
        }
      );

      this.selectedHour = null;
      this.outOfClassLoadService.getHour(this.selectedValueOutOfClassLoad.id).subscribe(
        data => {
          this.hours = data;
        }
      );
    }
  }

  add() {
    if (this.selectedTeacher != null && this.selectedValueOutOfClassLoad != null
       && this.selectedHour != null && this.selectedSemestr != null) {
      this.outOfClassLoadService.addOutOfClassLoad(this.selectedTeacher.id, this.selectedValueOutOfClassLoad.id,
        this.selectedHour, this.selectedSemestr).subscribe(
        data => {
          console.log(data);
          this.dialogRef.close();
        }
      );
    }
  }

}
