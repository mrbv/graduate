import {Component, OnInit} from '@angular/core';
import {TeachersDto} from "../dtos/TeachersDto";
import {MatTableDataSource} from "@angular/material/table";
import {OutOffClassLoadDto} from "../dtos/OutOffClassLoadDto";
import {OutOfClassLoadService} from "../services/out-of-class-load.service";
import {MatDialog} from "@angular/material/dialog";
import {OutOfClassLoadAddDialogComponent} from "./out-of-class-load-add-dialog/out-of-class-load-add-dialog.component";
import {RestService} from "../services/rest.service";

@Component({
  selector: 'out-of-class-load',
  templateUrl: './out-of-class-load.component.html',
  styleUrls: ['./out-of-class-load.component.css']
})
export class OutOffClassLoadComponent implements OnInit {

  displayedColumns: string[] = [
    'teacher',
    'outOffClassLoad',
    'hours',
    'semestr'
  ];

  dataSource = new MatTableDataSource<OutOffClassLoadDto>();

  teachers: TeachersDto[];
  selectedTeacher: TeachersDto;

  constructor(private outOfClassLoadService: OutOfClassLoadService, private restService: RestService,
              public dialog: MatDialog) { }

  ngOnInit(): void {
    this.outOfClassLoadService.getOutOfClassLoad().subscribe(
      data => {
        console.log('Тест:', data);
        console.log('Тест:', data[0].outOffClassLoad);
        console.log('Тест:', data[0]['outOfClassLoad']);
        this.dataSource.data = data;
      }
    );
    // фио преподавателей
    this.restService.getFullNameTeachers().subscribe(
      data => {
        this.teachers = data;
      }
    );
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(OutOfClassLoadAddDialogComponent, {
      width: '750px'
    });

    dialogRef.afterClosed().subscribe(() => {
      this.outOfClassLoadService.getOutOfClassLoad().subscribe(
        data => {
          this.dataSource.data = data;
        }
      );
    });
  }

  search(): void {
    if (this.selectedTeacher != null) {
      this.outOfClassLoadService.getOutOfClassLoadByFullNameTeacher(this.selectedTeacher.id).subscribe(
        data => {
          this.dataSource.data = data;
        }
      );
    } else {
      this.outOfClassLoadService.getOutOfClassLoad().subscribe(
        data => {
          this.dataSource.data = data;
        }
      );
    }
  }

  reset(): void {
    this.selectedTeacher = null;
    this.outOfClassLoadService.getOutOfClassLoad().subscribe(
      data => {
        this.dataSource.data = data;
      }
    );
  }

}
