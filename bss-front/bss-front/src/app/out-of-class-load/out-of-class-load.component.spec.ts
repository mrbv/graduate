import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OutOfClassLoadComponent} from './out-of-class-load.component';

describe('OutOfClassLoadComponent', () => {
  let component: OutOfClassLoadComponent;
  let fixture: ComponentFixture<OutOfClassLoadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutOfClassLoadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutOfClassLoadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
