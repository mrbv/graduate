import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {GroupsComponent} from './groups/groups.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from "@angular/common/http";
import {MatButtonModule} from "@angular/material/button";
import {MatTabsModule} from "@angular/material/tabs";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatDialogModule} from "@angular/material/dialog";
import {MatSelectModule} from "@angular/material/select";
import {ClassroomLoadComponent} from './classroom-load/classroom-load.component';
import {ClassroomLoadAddDialogComponent} from './classroom-load/classroom-load-add-dialog/classroom-load-add-dialog.component';
import {TeachersComponent} from './teachers/teachers.component';
import {OutOfClassWorkloadComponent} from './out-of-class-workload/out-of-class-workload.component';
import {ClassroomWorkloadComponent} from './classroom-workload/classroom-workload.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {OutOffClassLoadComponent} from './out-of-class-load/out-of-class-load.component';
import {OutOfClassLoadAddDialogComponent} from './out-of-class-load/out-of-class-load-add-dialog/out-of-class-load-add-dialog.component';
import {ClassroomLoadAutoComponent} from './classroom-load-auto/classroom-load-auto.component';
import {AvailableLoadComponent} from './available-load/available-load.component';
import {MatInputModule} from "@angular/material/input";

@NgModule({
  declarations: [
    AppComponent,
    GroupsComponent,
    ClassroomLoadComponent,
    ClassroomLoadAddDialogComponent,
    TeachersComponent,
    OutOfClassWorkloadComponent,
    ClassroomWorkloadComponent,
    OutOffClassLoadComponent,
    OutOfClassLoadAddDialogComponent,
    ClassroomLoadAutoComponent,
    AvailableLoadComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ClassroomLoadAddDialogComponent],
})
export class AppModule { }
