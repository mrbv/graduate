import {Component, OnInit, ViewChild} from '@angular/core';
import {RestService} from '../services/rest.service';
import {TeachersDto} from "../dtos/TeachersDto";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.css']
})
export class TeachersComponent implements OnInit {

  displayedColumns: string[] = [
    'fullName',
    'degree',
    'hours'
  ];
  dataSource = new MatTableDataSource<TeachersDto>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private restService: RestService) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.restService.getTeachers().subscribe(
      data => {
        this.dataSource.data = data;
      }
    );
  }

}
