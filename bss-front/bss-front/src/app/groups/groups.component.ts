import {Component, OnInit, ViewChild} from '@angular/core';
import {RestService} from '../services/rest.service';
import {GroupsInformationDto} from '../dtos/GroupsInformationDto';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";

@Component({
  selector: 'bss-group',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {

  displayedColumns: string[] = ['receiptYear', 'course', 'numberStudents', 'numberSubGroups'];
  dataSource = new MatTableDataSource<GroupsInformationDto>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private restService: RestService) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.restService.getGroupsInformation().subscribe(
      data => {
        this.dataSource.data = data;
      }
    );
  }

}
