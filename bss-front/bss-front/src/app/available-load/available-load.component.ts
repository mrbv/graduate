import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {AvailableLoadDto} from "../dtos/AvailableLoadDto";
import {MatPaginator} from "@angular/material/paginator";
import {RestService} from "../services/rest.service";

@Component({
  selector: 'available-load',
  templateUrl: './available-load.component.html',
  styleUrls: ['./available-load.component.css']
})
export class AvailableLoadComponent implements OnInit {

  displayedColumns: string[] = [
    'teacher',
    'classroomWorkload',
  ];

  dataSource = new MatTableDataSource<AvailableLoadDto>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private restService: RestService) { }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.restService.getAvailableLoad().subscribe(
      data => {
        console.log(data);
      this.dataSource.data = data;
    })
  }

}
