import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {OutOffClassLoadDto} from "../dtos/OutOffClassLoadDto";
import {OutOfClassWorkloadDto} from "../dtos/OutOfClassWorkloadDto";

@Injectable({
  providedIn: 'root'
})
export class OutOfClassLoadService {

  readonly baseUrl: string;
  readonly outOfClassLoadUrl: string;
  readonly outOfClassWorkloadNameUrl: string;
  readonly hoursUrl: string;
  readonly semestrUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = 'http://localhost:8080/out-of-class/';
    this.outOfClassLoadUrl = this.baseUrl + 'load';
    this.outOfClassWorkloadNameUrl = this.baseUrl + 'workload/name';
    this.hoursUrl = this.baseUrl + 'workload/hours';
    this.semestrUrl = this.baseUrl + 'workload/semestr';
  }

  public getOutOfClassLoad(): Observable<OutOffClassLoadDto[]> {
    return this.http.get<OutOffClassLoadDto[]>(this.outOfClassLoadUrl);
  }

  public getOutOfClassLoadByFullNameTeacher(teacherId : number): Observable<OutOffClassLoadDto[]> {
    return this.http.get<OutOffClassLoadDto[]>(this.outOfClassLoadUrl + '/' + teacherId);
  }

  public getNameOutOfClassWorkload(): Observable<OutOfClassWorkloadDto[]> {
    return this.http.get<OutOfClassWorkloadDto[]>(this.outOfClassWorkloadNameUrl);
  }

  public getHour(outOfClassWorkloadNameId: number): Observable<number> {
    const url = this.hoursUrl + '/' + outOfClassWorkloadNameId;
    return this.http.get<number>(url);
  }

  public getSemestr(outOfClassWorkloadNameId: number): Observable<number[]> {
    const url = this.semestrUrl + '/' + outOfClassWorkloadNameId;
    return this.http.get<number[]>(url);
  }

  public addOutOfClassLoad(teacherId : number, audienceWorkloadId : number, hour : number, semestr : number) : Observable<any> {
    const url = this.outOfClassLoadUrl + '/add/' + teacherId + '/' + audienceWorkloadId + '/' + hour + '/' + semestr;
    return this.http.get(url);
  }

}
