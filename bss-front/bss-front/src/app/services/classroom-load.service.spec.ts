import {TestBed} from '@angular/core/testing';

import {ClassroomLoadService} from './classroom-load.service';

describe('ClassroomLoadService', () => {
  let service: ClassroomLoadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ClassroomLoadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
