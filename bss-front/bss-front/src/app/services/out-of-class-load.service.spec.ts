import {TestBed} from '@angular/core/testing';

import {OutOfClassLoadService} from './out-of-class-load.service';

describe('OutOfClassLoadService', () => {
  let service: OutOfClassLoadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OutOfClassLoadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
