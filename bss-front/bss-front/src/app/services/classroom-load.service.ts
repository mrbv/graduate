import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {ClassroomLoadDto} from "../dtos/ClassroomLoadDto";
import {HttpClient} from "@angular/common/http";
import {ClassroomWorkloadDto} from "../dtos/ClassroomWorkloadDto";

@Injectable({
  providedIn: 'root'
})
export class ClassroomLoadService {

  readonly baseUrl: string;
  readonly classroomLoadUrl: string;
  readonly classroomWorkloadUrl: string;
  readonly classroomWorkloadNameUrl: string;
  readonly autoClassroomLoadUrl: string;
  readonly hoursUrl: string;
  readonly semestrUrl: string;
  readonly autoUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = 'http://localhost:8080/classroom/';
    this.classroomLoadUrl = this.baseUrl + 'load';
    this.classroomWorkloadUrl = this.baseUrl + 'workload';
    this.classroomWorkloadNameUrl = this.baseUrl + 'workload/name';
    this.autoClassroomLoadUrl = this.baseUrl + 'auto-load';
    this.hoursUrl = this.baseUrl + 'workload/hours';
    this.semestrUrl = this.baseUrl + 'workload/semestr';
    this.autoUrl = this.baseUrl + 'auto';
  }

  public getClassroomLoad(): Observable<ClassroomLoadDto[]> {
    return this.http.get<ClassroomLoadDto[]>(this.classroomLoadUrl);
  }

  public getClassroomWorkload(): Observable<ClassroomWorkloadDto[]> {
    return this.http.get<ClassroomWorkloadDto[]>(this.classroomWorkloadUrl);
  }

  public getClassroomLoadByFullNameTeacherAndSemestr(teacherId : number, semestr: string): Observable<ClassroomLoadDto[]> {
    console.log("teacherId = ", teacherId);
    return this.http.get<ClassroomLoadDto[]>(this.classroomLoadUrl + '/' + teacherId + '/' + semestr);
  }

  public getNameAudienceWorkload(): Observable<ClassroomWorkloadDto[]> {
    return this.http.get<ClassroomWorkloadDto[]>(this.classroomWorkloadNameUrl);
  }

  public getHour(audienceWorkloadNameId: number, typeLoad: number): Observable<number> {
    const url = this.hoursUrl + '/' + audienceWorkloadNameId + '/' + typeLoad;
    return this.http.get<number>(url);
  }

  public getSemestr(audienceWorkloadNameId: number): Observable<number[]> {
    const url = this.semestrUrl + '/' + audienceWorkloadNameId;
    return this.http.get<number[]>(url);
  }

  public addClassroomLoad(teacherId: number, audienceWorkloadId: number, typeLoadId: string, hour: number,
                          semestr: number): Observable<any> {
    const url = this.classroomLoadUrl + '/add/' + teacherId + '/' + audienceWorkloadId + '/' + typeLoadId + '/' + hour
      + '/' + semestr;
    return this.http.get(url);
  }

  public autoLoad() : Observable<any> {
    return this.http.get(this.autoUrl);
  }

  public getAutoClassroomLoad(): Observable<ClassroomLoadDto[]> {
    return this.http.get<ClassroomLoadDto[]>(this.autoClassroomLoadUrl);
  }

  public deleteAll() : Observable<any> {
    return this.http.get(this.autoUrl + '/delete-all');
  }

  public getAutoClassroomLoadByFullNameTeacher(teacherId : number): Observable<ClassroomLoadDto[]> {
    return this.http.get<ClassroomLoadDto[]>(this.autoUrl + '/' + teacherId);
  }

}
