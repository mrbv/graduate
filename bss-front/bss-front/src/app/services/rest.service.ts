import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {GroupsInformationDto} from "../dtos/GroupsInformationDto";
import {OutOfClassWorkloadDto} from "../dtos/OutOfClassWorkloadDto";
import {TeachersDto} from "../dtos/TeachersDto";
import {AvailableLoadDto} from "../dtos/AvailableLoadDto";

@Injectable({
  providedIn: 'root'
})
export class RestService {

  readonly baseUrl: string;
  readonly groupsUrl: string;
  readonly outOfClassWorkloadUrl: string;
  readonly teachersUrl: string;
  readonly audienceWorkloadTypeLoadUrl: string;
  readonly teachersFullNameUrl: string;
  readonly availableUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = 'http://localhost:8080/';
    this.groupsUrl = this.baseUrl + 'students/group/information';
    this.outOfClassWorkloadUrl = this.baseUrl + 'out/of/class/workload';
    this.teachersUrl = this.baseUrl + 'teachers';
    this.audienceWorkloadTypeLoadUrl = this.baseUrl + 'audience/workload/type/load';
    this.teachersFullNameUrl = this.baseUrl + 'teachers/full/name';
    this.availableUrl = this.baseUrl + 'available'
  }

  public getGroupsInformation(): Observable<GroupsInformationDto[]> {
    return this.http.get<GroupsInformationDto[]>(this.groupsUrl);
  }

  public getOutOfClassWorkload(): Observable<OutOfClassWorkloadDto[]> {
    return this.http.get<OutOfClassWorkloadDto[]>(this.outOfClassWorkloadUrl);
  }

  public getTeachers(): Observable<TeachersDto[]> {
    return this.http.get<TeachersDto[]>(this.teachersUrl);
  }

  public getFullNameTeachers(): Observable<TeachersDto[]> {
    return this.http.get<TeachersDto[]>(this.teachersFullNameUrl);
  }

  public getAvailableLoad(): Observable<AvailableLoadDto[]> {
    return this.http.get<AvailableLoadDto[]>(this.availableUrl);
  }

}
