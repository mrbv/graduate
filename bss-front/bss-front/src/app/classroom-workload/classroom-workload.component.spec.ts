import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ClassroomWorkloadComponent} from './classroom-workload.component';

describe('ClassroomWorkloadComponent', () => {
  let component: ClassroomWorkloadComponent;
  let fixture: ComponentFixture<ClassroomWorkloadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassroomWorkloadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassroomWorkloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
