import {Component, OnInit, ViewChild} from '@angular/core';
import {ClassroomWorkloadDto} from "../dtos/ClassroomWorkloadDto";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {ClassroomLoadService} from "../services/classroom-load.service";

@Component({
  selector: 'classroom-workload',
  templateUrl: './classroom-workload.component.html',
  styleUrls: ['./classroom-workload.component.css']
})
export class ClassroomWorkloadComponent implements OnInit {

  displayedColumns: string[] = [
    'curriculumDesignation',
    'profileProgramSpecializationFocus',
    'emptyColumn',
    'nameOfDisciplines',
    'kindOfDisciplines',
    'laborInput',
    'independentWork',
    'auditorsHours',
    'zet',
    'lectures',
    'practice',
    'laboratoryLessons',
    'semestr',
    'offset',
    'exam'
  ];
  dataSource = new MatTableDataSource<ClassroomWorkloadDto>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private restService: ClassroomLoadService) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.restService.getClassroomWorkload().subscribe(
      data => {
        this.dataSource.data = data;
      }
    );
  }

}
