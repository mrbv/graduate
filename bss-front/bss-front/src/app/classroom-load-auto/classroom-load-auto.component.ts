import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {ClassroomLoadDto} from "../dtos/ClassroomLoadDto";
import {MatPaginator} from "@angular/material/paginator";
import {TeachersDto} from "../dtos/TeachersDto";
import {ClassroomLoadService} from "../services/classroom-load.service";
import {RestService} from "../services/rest.service";
import {MatDialog} from "@angular/material/dialog";
import {ClassroomLoadAddDialogComponent} from "../classroom-load/classroom-load-add-dialog/classroom-load-add-dialog.component";

@Component({
  selector: 'classroom-load-auto',
  templateUrl: './classroom-load-auto.component.html',
  styleUrls: ['./classroom-load-auto.component.css']
})
export class ClassroomLoadAutoComponent implements OnInit {

  displayedColumns: string[] = [
    'teacher',
    'classroomWorkload',
    'hours',
    'typeLoad',
    'semestr'
  ];

  dataSource = new MatTableDataSource<ClassroomLoadDto>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  teachers: TeachersDto[];
  selectedTeacher: TeachersDto;

  totalHours: number;

  constructor(private classroomLoadService: ClassroomLoadService, private restService: RestService,
              public dialog: MatDialog) {
    this.totalHours = 0;
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.classroomLoadService.getAutoClassroomLoad().subscribe(
      data => {
        console.log(data);
        this.dataSource.data = data;
        this.totalHours = 0;
        data.forEach((item) => {
          this.totalHours += item.hours;
        })
      }
    );

    // фио преподавателей
    this.restService.getFullNameTeachers().subscribe(
      data => {
        this.teachers = data;
      }
    );
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ClassroomLoadAddDialogComponent, {
      width: '750px'
    });

    dialogRef.afterClosed().subscribe(() => {
      this.classroomLoadService.getAutoClassroomLoad().subscribe(
        data => {
          this.dataSource.data = data;
        }
      );
    });
  }

  search(): void {
    if (this.selectedTeacher != null) {
      this.classroomLoadService.getAutoClassroomLoadByFullNameTeacher(this.selectedTeacher.id).subscribe(
        data => {
          this.dataSource.data = data;
          this.totalHours = 0;
          data.forEach((item) => {
            this.totalHours += item.hours;
          })
        }
      );
    } else {
      this.classroomLoadService.getAutoClassroomLoad().subscribe(
        data => {
          this.dataSource.data = data;
          this.totalHours = 0;
          data.forEach((item) => {
            this.totalHours += item.hours;
          })
        }
      );
    }
  }

  reset(): void {
    this.selectedTeacher = null;
    this.classroomLoadService.getAutoClassroomLoad().subscribe(
      data => {
        this.dataSource.data = data;
        this.totalHours = 0;
        data.forEach((item) => {
          this.totalHours += item.hours;
        })
      }
    );
  }

  auto(): void {
    this.classroomLoadService.autoLoad().subscribe(
      data => {
        console.log(data);
        this.classroomLoadService.getAutoClassroomLoad().subscribe(
          data => {
            this.dataSource.data = data;
            this.totalHours = 0;
            data.forEach((item) => {
              this.totalHours += item.hours;
            })
          }
        );
      }
    );
  }

  deleteAll(): void {
    this.classroomLoadService.deleteAll().subscribe(
      data => {
        console.log(data);
        this.classroomLoadService.getAutoClassroomLoad().subscribe(
          data => {
            this.dataSource.data = data;
            this.totalHours = 0;
          }
        );
      }
    );
  }

}
