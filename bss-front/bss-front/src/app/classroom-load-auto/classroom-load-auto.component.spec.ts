import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ClassroomLoadAutoComponent} from './classroom-load-auto.component';

describe('ClassroomLoadAutoComponent', () => {
  let component: ClassroomLoadAutoComponent;
  let fixture: ComponentFixture<ClassroomLoadAutoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassroomLoadAutoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassroomLoadAutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
