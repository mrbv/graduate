import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OutOfClassWorkloadComponent} from './out-of-class-workload.component';

describe('OutOfClassWorkloadComponent', () => {
  let component: OutOfClassWorkloadComponent;
  let fixture: ComponentFixture<OutOfClassWorkloadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutOfClassWorkloadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutOfClassWorkloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
