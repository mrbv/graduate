import {Component, OnInit, ViewChild} from '@angular/core';
import {RestService} from '../services/rest.service';
import {OutOfClassWorkloadDto} from "../dtos/OutOfClassWorkloadDto";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'out-of-class-workload',
  templateUrl: './out-of-class-workload.component.html',
  styleUrls: ['./out-of-class-workload.component.css']
})
export class OutOfClassWorkloadComponent implements OnInit {

  displayedColumns: string[] = [
    'curriculumDesignation',
    'profileProgramSpecializationFocus',
    'nameOfStudy',
    'kindTrainingLoad',
    'course',
    'semestr',
    'numberGroups',
    'numberSubgroups',
    'days',
    'strength',
    'weeks',
    'hours',
    'zet',
  ];
  dataSource = new MatTableDataSource<OutOfClassWorkloadDto>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private restService: RestService) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.restService.getOutOfClassWorkload().subscribe(
      data => {
        console.log(data);
        this.dataSource.data = data;
      }
    );
  }

}
