import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ClassroomLoadAddDialogComponent} from './classroom-load-add-dialog.component';

describe('ClassroomLoadAddDialogComponent', () => {
  let component: ClassroomLoadAddDialogComponent;
  let fixture: ComponentFixture<ClassroomLoadAddDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassroomLoadAddDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassroomLoadAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
