import {Component, OnInit} from '@angular/core';
import {ClassroomLoadService} from "../../services/classroom-load.service";
import {FormControl} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {TypeLoadDto, typeLoadStub} from "../../dtos/TypeLoadDto";
import {ClassroomWorkloadDto} from "../../dtos/ClassroomWorkloadDto";
import {TeachersDto} from "../../dtos/TeachersDto";
import {RestService} from "../../services/rest.service";

@Component({
  selector: 'app-classroom-load-add-dialog',
  templateUrl: './classroom-load-add-dialog.component.html',
  styleUrls: ['./classroom-load-add-dialog.component.css']
})
export class ClassroomLoadAddDialogComponent implements OnInit {

  teachers: TeachersDto[];
  classroomWorkload: ClassroomWorkloadDto[];
  // typeLoad: string[] = ["Лекции", "Практические", "Лабораторные"];
  typeLoad = typeLoadStub;
  hours: number;
  semestr: number[];

  classroomWorkloadControl = new FormControl();
  typeLoadControl = new FormControl();

  selectedValueClassroomWorkload: ClassroomWorkloadDto;
  selectedValueTypeLoad: TypeLoadDto;
  selectedTeacher: TeachersDto;
  selectedHour: number;
  selectedSemestr: number;

  constructor(private classroomLoadService: ClassroomLoadService, private restService: RestService,
              public dialogRef: MatDialogRef<ClassroomLoadAddDialogComponent>) { }

  ngOnInit() {
    // фио преподавателей
    this.restService.getFullNameTeachers().subscribe(
      data => {
        this.teachers = data;
      }
    );

    // наименования дисциплин
    this.classroomLoadService.getNameAudienceWorkload().subscribe(
      data => {
        this.classroomWorkload = data;
      }
    );
  }

  classroomWorkloadOrTypeLoadChange() {
    // запрос часов по выбранным параметрам
    if (this.classroomWorkloadControl.dirty) {
      this.classroomLoadService.getSemestr(this.selectedValueClassroomWorkload.id).subscribe(
        data => {
          this.semestr = data;
        }
      );
      if (this.typeLoadControl.dirty) {
        this.classroomLoadService.getHour(this.selectedValueClassroomWorkload.id, this.selectedValueTypeLoad.id).subscribe(
          data => {
            this.hours = data;
          }
        );
      }
    }
  }

  add() {
    if (this.selectedTeacher != null && this.selectedValueClassroomWorkload != null
      && this.selectedValueTypeLoad != null && this.selectedHour != null && this.selectedSemestr != null) {
      this.classroomLoadService.addClassroomLoad(this.selectedTeacher.id, this.selectedValueClassroomWorkload.id,
        this.selectedValueTypeLoad.nameTypeLoad, this.selectedHour, this.selectedSemestr).subscribe(
          data => {
            console.log(data);
            this.dialogRef.close();
          }
      );
    }
  }

}
