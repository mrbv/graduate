import {Component, OnInit, ViewChild} from '@angular/core';
import {ClassroomLoadService} from '../services/classroom-load.service';
import {ClassroomLoadDto} from "../dtos/ClassroomLoadDto";
import {ClassroomLoadAddDialogComponent} from "./classroom-load-add-dialog/classroom-load-add-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {TeachersDto} from "../dtos/TeachersDto";
import {RestService} from "../services/rest.service";

@Component({
  selector: 'classroom-load',
  templateUrl: './classroom-load.component.html',
  styleUrls: ['./classroom-load.component.css']
})
export class ClassroomLoadComponent implements OnInit {

  displayedColumns: string[] = [
    'teacher',
    'classroomWorkload',
    'hours',
    'typeLoad',
    'semestr'
  ];

  dataSource = new MatTableDataSource<ClassroomLoadDto>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  teachers: TeachersDto[];
  selectedTeacher: TeachersDto;

  totalHours: number;
  semestrs: string[] = ['Четный', 'Нечетный'];

  selectedSemestr: string;

  constructor(private classroomLoadService: ClassroomLoadService, private restService: RestService,
              public dialog: MatDialog) {
    this.totalHours = 0;
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.classroomLoadService.getClassroomLoad().subscribe(
      data => {
        console.log(data);
        this.dataSource.data = data;
        this.totalHours = 0;
        data.forEach((item) => {
          this.totalHours += item.hours;
        });
      }
    );

    // фио преподавателей
    this.restService.getFullNameTeachers().subscribe(
      data => {
        this.teachers = data;
      }
    );
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ClassroomLoadAddDialogComponent, {
      width: '750px'
    });

    dialogRef.afterClosed().subscribe(() => {
      this.classroomLoadService.getClassroomLoad().subscribe(
        data => {
          this.dataSource.data = data;
          this.totalHours = 0;
          data.forEach((item) => {
            this.totalHours += item.hours;
          });
        }
      );
    });
  }

  search(): void {
    let teacherId = -1;
    if (this.selectedTeacher != null) {
      teacherId = this.selectedTeacher.id;
    }

    let semestr = '';
    if (this.selectedSemestr != null) {
      semestr = this.selectedSemestr;
    }
    this.classroomLoadService.getClassroomLoadByFullNameTeacherAndSemestr(teacherId, semestr).subscribe(
        data => {
          this.dataSource.data = data;
          this.totalHours = 0;
          data.forEach((item) => {
            this.totalHours += item.hours;
          });
        }
      );
    // } else {
    //   this.classroomLoadService.getClassroomLoad().subscribe(
    //     data => {
    //       this.dataSource.data = data;
    //       this.totalHours = 0;
    //       data.forEach((item) => {
    //         this.totalHours += item.hours;
    //       })
    //     }
    //   );
    // }
  }

  reset(): void {
    this.selectedTeacher = null;
    this.selectedSemestr = null;
    this.classroomLoadService.getClassroomLoad().subscribe(
      data => {
        this.dataSource.data = data;
        this.totalHours = 0;
        data.forEach((item) => {
          this.totalHours += item.hours;
        });
      }
    );
  }

}
