import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ClassroomLoadComponent} from './classroom-load.component';

describe('ClassroomLoadComponent', () => {
  let component: ClassroomLoadComponent;
  let fixture: ComponentFixture<ClassroomLoadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassroomLoadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassroomLoadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
